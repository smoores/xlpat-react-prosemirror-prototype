# Cross-platform React ProseMirror Prototype

Suuuuper simple tech demo for running ProseMirror "natively" on mobile. Natively is in quotes because while this produces a native app, that app renders the ProseMirror editor in a WebView. This is necessary to avoid reimplementing the entirety of prosemirror-view, which relies entirely on browser APIs.

## Running it

This project uses yarn for package management. Make sure you have yarn installed, then run `yarn` in this folder to install all packages.

This project uses [Expo](https://expo.dev/) to run React Native.

To run on web:

```
yarn dev:web
```

This will open up your browser to the running server. This isn't really anything special -- ProseMirror already runs just fine in the browser -- but it's a demonstration of how this approach can be truly cross platform. Technically the Editor is inside of an iframe here.

To run on iOS:

Install the Expo Go app on an iPhone or iPad.

Run `yarn dev:ios`. You should see a QR code; if you scan it with your device's camera, it will open the app in Expo Go.

## Making changes

The codebase is written in Typescript. Everything in the src directory will trigger hot reload of the clients.

The source for the webview is currently injected rather hackily; the source code lives in `editorsrc`. Running `yarn build:editor` will compile and bundle the code in that directory, and output it as a base64-encoded string into `src/editor.ts`, where it will be picked up by the primary app and injected into the WebView. The change payload is often too big for Expo Go, so you might have to restart the server after building the editor.

## How it works

We use [`react-native-webview`](https://github.com/react-native-webview/react-native-webview) to render a WebView component, and [`react-native-web-webview`](https://github.com/react-native-web-community/react-native-web-webview) to swap it out for an iframe on web. There's some goofy environment checking code in the `src/app/story/[id].tsx` file, because `react-native-web-webview` doesn't perfectly match React Native Web's API or implementation.

The state lives in the React Native components; it's serialized and passed down into the WebView after each change, where we rerender the React tree in the WebView with the new state. When changes are dispatched in the WebView, we post the updated state, serialized, back up to the parent component.
