import { useLocalSearchParams } from "expo-router";
import { Schema } from "prosemirror-model";
import { EditorState } from "prosemirror-state";
import { MutableRefObject, useLayoutEffect, useRef, useState } from "react";
import {
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  View,
} from "react-native";

import { injectedJavascript } from "@/editor";
import { WebView } from "@/react-native-webview";

const schema = new Schema({
  nodes: {
    doc: { content: "paragraph+" },
    text: { inline: true },
    paragraph: {
      content: "text*",
      toDOM() {
        return ["p", 0];
      },
    },
  },
  marks: {
    em: {
      toDOM() {
        return ["em", 0];
      },
    },
    strong: {
      toDOM() {
        return ["strong", 0];
      },
    },
  },
});

const editorState = EditorState.create({
  schema,
});

export default function App() {
  const { id } = useLocalSearchParams();
  const [state, setState] = useState(editorState);

  const webviewRef = useRef<
    | WebView
    | {
        frameRef: HTMLIFrameElement;
        postMessage: (typeof window)["postMessage"];
      }
  >(null);

  useLayoutEffect(() => {
    if (webviewRef.current && "injectJavaScript" in webviewRef.current) {
      webviewRef.current.injectJavaScript(`
        window.renderEditor(${JSON.stringify(state.toJSON())})
      `);
    } else {
      webviewRef.current?.postMessage(state.toJSON());
    }
  }, [state]);

  return (
    <SafeAreaView style={{ flex: 1, padding: StatusBar.currentHeight }}>
      <ScrollView>
        <View style={{ flex: 1, alignItems: "center" }}>
          <Text
            style={{
              flex: 1,
              fontSize: 16,
            }}
          >
            Story {id}
          </Text>
        </View>
        <WebView
          ref={webviewRef as MutableRefObject<WebView>}
          style={{ flex: Platform.OS === "web" ? 1 : 0, height: 1000 }}
          originWhitelist={["*"]}
          onMessage={(event) => {
            if (event.nativeEvent.data.source?.startsWith("react-devtools"))
              return;
            const newState = EditorState.fromJSON(
              { schema },
              typeof event.nativeEvent.data === "string"
                ? JSON.parse(event.nativeEvent.data)
                : event.nativeEvent.data
            );
            setState(newState);
          }}
          scrollEnabled={false}
          injectedJavaScript={injectedJavascript}
          // The first renderEditor call _has_ to be injected
          // on iOS and Android, for some reason, but the web
          // implementation runs this code every time, which messes up
          // the effect above.
          {...(Platform.OS !== "web" && {
            injectedJavaScript: `
            ${injectedJavascript}
            window.renderEditor(${JSON.stringify(state.toJSON())});
            `,
          })}
          source={{
            html: `
            <html>
            <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <style>
            html, body, #__next {
              -webkit-overflow-scrolling: touch;
            }
            #__next {
              display: flex;
              flex-direction: column;
              height: 100%;
            }
            html {
              scroll-behavior: smooth;
              -webkit-text-size-adjust: 100%;
            }
            body {
              /* Allows you to scroll below the viewport; default value is visible */
              overflow-y: auto;
              overscroll-behavior-y: none;
              text-rendering: optimizeLegibility;
              -webkit-font-smoothing: antialiased;
              -moz-osx-font-smoothing: grayscale;
              -ms-overflow-style: scrollbar;
            }
            .ProseMirror {
              border: thin solid black;
              border-radius: 0.25rem;
              padding: 1rem;
              outline: none;
              min-height: 400px;
            }
            </style>
            </head>
            <body>
            <main id="editor"></main>
            </body>
            </html>
          `,
          }}
          webviewDebuggingEnabled
        />
      </ScrollView>
    </SafeAreaView>
  );
}
