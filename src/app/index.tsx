import { Link } from "expo-router";
import { FlatList, Text, View } from "react-native";

type StoryLinkProps = {
  id: string;
};

function StoryLink({ id }: StoryLinkProps) {
  return (
    <View>
      <Link href={`/story/${id}`}>Story 1</Link>
    </View>
  );
}

export default function Page() {
  return (
    <View>
      <Text>Home page</Text>
      <FlatList
        data={[
          {
            id: "1",
          },
        ]}
        renderItem={({ item }) => <StoryLink id={item.id} />}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}
