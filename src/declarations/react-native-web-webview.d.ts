import { WebView } from "react-native-webview";

declare module "react-native-web-webview" {
  export const WebView: WebView;
}
