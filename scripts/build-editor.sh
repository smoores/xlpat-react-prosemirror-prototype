#!/usr/bin/env bash

echo 'yarn webpack build --entry ./editorsrc/main.tsx -c ./editorsrc/webpack.config.js'

yarn webpack build --entry ./editorsrc/main.tsx -c ./editorsrc/webpack.config.js -o $(pwd)/editorsrc/bundle

echo 'base64 encode'

# echo 'import { decode } from "base-64";' > src/editor.ts
# echo '' >> src/editor.ts
# echo 'export const injectedJavascript = decode(`' >> src/editor.ts
# base64 < editorsrc/bundle/main.js >> src/editor.ts
# echo '`);' >> src/editor.ts

printf "import { decode } from \"base-64\";\n\nexport const injectedJavascript = decode(\`\n$(base64 < editorsrc/bundle/main.js)\n\`);" > src/editor.ts

rm -rf ./editorsrc/bundle
