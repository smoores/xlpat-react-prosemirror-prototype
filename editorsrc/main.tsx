import { EditorView } from "@nytimes/react-prosemirror";
import { baseKeymap, toggleMark } from "prosemirror-commands";
import { keymap } from "prosemirror-keymap";
import { Schema } from "prosemirror-model";
import { EditorState } from "prosemirror-state";
import React from "react";
import { createRoot } from "react-dom/client";

const schema = new Schema({
  nodes: {
    doc: { content: "paragraph+" },
    text: { inline: true },
    paragraph: {
      content: "text*",
      toDOM() {
        return ["p", 0];
      },
    },
  },
  marks: {
    em: {
      toDOM() {
        return ["em", 0];
      },
    },
    strong: {
      toDOM() {
        return ["strong", 0];
      },
    },
  },
});

// const debugViewPlugin = new Plugin({
//   view(view) {
//     const debugDiv = document.createElement("div");
//     const debugDomP = document.createElement("p");
//     const debugStateP = document.createElement("p");
//     const debugSelP = document.createElement("p");
//     debugDiv.appendChild(debugDomP);
//     debugDiv.appendChild(debugStateP);
//     debugDiv.appendChild(debugSelP);
//     debugDiv.style.position = "absolute";
//     debugDiv.style.bottom = "50px";
//     view.dom.parentElement!.appendChild(debugDiv);
//     debugDomP.innerText = view.dom.innerHTML;
//     debugStateP.innerText = `"${view.state.doc.textContent}"`;
//     debugSelP.innerText = view.state.selection.anchor.toString();
//     return {
//       update(view) {
//         debugDomP.innerText = view.dom.innerHTML;
//         debugStateP.innerText = `"${view.state.doc.textContent}"`;
//         debugSelP.innerText = view.state.selection.anchor.toString();
//       },
//       destroy() {
//         view.dom.parentElement!.removeChild(debugDiv);
//       },
//     };
//   },
// });

const plugins = [
  keymap({
    ...baseKeymap,
    "Mod-i": toggleMark(schema.marks.em),
    "Mod-b": toggleMark(schema.marks.strong),
  }),
  // debugViewPlugin
];

const editorDOM = document.getElementById("editor")!;

type Props = {
  stateJson: any;
  source?: MessageEventSource | null;
};

function Editor({ stateJson, source }: Props) {
  const editorState = EditorState.fromJSON({ schema }, stateJson);

  return (
    <EditorView
      className="ProseMirror"
      state={editorState}
      dispatchTransaction={function (tr) {
        // `this` is bound by the EditorView component
        // eslint-disable-next-line react/no-this-in-sfc
        const nextState = this.state.apply(tr).toJSON();
        if ("ReactNativeWebView" in window) {
          (
            window.ReactNativeWebView as {
              postMessage: (typeof window)["postMessage"];
            }
          ).postMessage(JSON.stringify(nextState));
        } else {
          source?.postMessage(nextState);
        }
      }}
      plugins={plugins}
    />
  );
}

const root = createRoot(editorDOM);

declare global {
  interface Window {
    renderEditor(stateJson: any, source?: MessageEventSource | null): void;
  }
}

window.renderEditor = function renderEditor(
  stateJson: any,
  source?: MessageEventSource | null
) {
  console.log("rendering");
  root.render(<Editor stateJson={stateJson} source={source ?? null} />);
};

window.addEventListener("message", (event) => {
  window.renderEditor(event.data, event.source);
});
