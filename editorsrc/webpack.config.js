module.exports = {
  resolve: {
    extensions: ['.ts', '.tsx', '...']
  },
  module: {
    rules: [
      {
        test: /\.m?[jt]sx?$/,
        exclude: /(node_modules)/,
        use: {
          loader: "swc-loader",
          options: {
            jsc: {
              parser: {
                jsx: true,
                syntax: 'typescript'
              },
              transform: {
                react: {
                  runtime: "classic"
                }
              }
            }
          }
        }
      }
    ]
  }
}
